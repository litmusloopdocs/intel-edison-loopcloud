# edison-arduino

The **Intel Edison** is a tiny computer-on-module offered by Intel as a development system for wearable devices[1] and Internet of Things devices. 


## Overview 

The project demonstrates the implementation of MQTT.**Message Queue Telemetry Transport (MQTT)** is an extremely simple and lightweight messaging protocol, designed for constrained devices and low bandwidth,
high latency and unreliable networks. The protocol uses **publish/subscribe** communication pattern and is used for machine to machine communication.It plays an important role in the internet of things.
MQTT protocol works on the **TCP/IP connection**.

The library provides an example of publish and subscribe messaging with a server that supports MQTT using Intel Edison- Arduino.
 
*Features provided by the client library*

* *Connect* the device to any IP network using Ethernet, Wi-Fi, 4G/LTE
* *Publish* any message to the MQTT server in standard JSON format on a user soecified topic
* *Subscribe* data from the server to the device on a specific topic
* *Unsubscribe* the topic to stop streaming data to the cloud
* *Disconnect* the device from any network connectivity.

The following Table shows status of the client and the server when the above functions are implemented.

|Function	|  Server Status   |    Client Status
----------------|------------------|---------------
|Looopmq.connect | Connected       | Connected|
|Loopmq.publish |Connected |	Connected|
|Loopmq.subscribe |	Connected |	Connected|
|Loopmq.unsubscribe |	Connected |	Disconnected|
|Loopmq.disconnect |	Disconnected |	Disconnected|


## Getting Started with Intel Edison- Arduino

A step by step instruction to setup the Intel Edison board can be followed at the [link](https://software.intel.com/en-us/iot/library/edison-getting-started)

Basic steps to start using the Edison board are given below:

* STEP 1: Assemble and connect the board as shown in the below.
![alt text](https://bytebucket.org/litmusloopdocs/intel-edison-loopcloud/raw/master/extras/InitialSetup.JPG) 

* STEP 2: Run the SETUP tool

![alt text](https://bytebucket.org/litmusloopdocs/intel-edison-loopcloud/raw/master/extras/IntelEdisonSoftwareSetup.JPG)

The device is connected to the network by the use of GUI Connect Wi-Fi given in the setup tool
![Alt text](https://bytebucket.org/litmusloopdocs/intel-edison-loopcloud/raw/master/extras/NetworkSetup.JPG)

* STEP 3: Install Arduino IDE and install Edison board from the board manager as shown in the [figure](https://gitlabinternal.litmusloop.com/embedded/edison-arduino/raw/development/extras/images/Arduino_board.jpg) below. 
![alt text](https://bytebucket.org/litmusloopdocs/intel-edison-loopcloud/raw/master/extras/ArduinoBoard.jpg)

* STEP 4: Install the library or open the mqtt_basic_edison_arduino.ino file from the examples.
>**NOTE:**
 Install the [Grove - LCD RGB Backlight](https://github.com/Seeed-Studio/Grove_LCD_RGB_Backlight.git) library for the code to compile

* STEP 5: After code is compiled and flashed to Intel Edison arduino, you should see the messages published.
In the below figure the topic is "automation" and the payload or the data is "Loop 2.0".

Any data that is published to the device can be observed on the LCD in the form of

"topic" : 
  "data published"

![alt text](https://bytebucket.org/litmusloopdocs/intel-edison-loopcloud/raw/master/extras/output.JPG)



## Configuration

The user need to define a list of parameters in order to connect the device to a server in a secured manner.

Below are the list of minimum definitions required by the user to send data to the cloud.

```
#define port_number 1883                             // Port number
#define server "loopdocker1.cloudapp.net"            // Server name
#define clientID "arduinoclient"                     // ClientID
#define password "password"                          // password
#define userID "admin"                               // username 
#define subTOPIC "intel_edison/loop1"                // Subscribe on this topic to get the data
#define pubTopic "intel_edison/loop2"                // Publish on this tpoic to send data or command to device 
```

The mac address and/or IP is required for the device to connect to network.

```
byte mac[] = {0x78,0x4b,0x87,0xaa,0x0a,0x15}; // Mac address of the device
//IPAddress ip(172, 20, 10, 5);    // IP addresss of the device
```

## Functions

1.***loopmq.connect (client ID)***
This function is used to connect the device or the client to the client ID specified by the user.

```
if (loopmq.connect(c)) {
      Serial.println ("connected"); 
```       

2.***loopmq.connect (client ID, username, password)***
Checks for the username and password specified by the user to connect the device to the network.

```
if (loopmq.connect(c, user, pass))
```
 

3.***loopmq.publish (topic, data)***

This function is used to publish data in string format to the topic specified by the user. 

In this example numbers form 1-100 are published in a continuous loop. You can also monitor the data that is send to the cloud, serially.

```
loopmq.publish (p,buffer);        // Publish message to the server
```


4.***loopmq.subscribe (topic)***
This function is used to subscribe to a topic to which data will be published to the device.

```
loopmq.subscribe(s);                    // Subscribe to a topic
```

5.***loopmq.unsubscribe (topic)***
This function is used to unsubscribe the device from the server. Calling this function will stop sending messages from the device to the server.

```
// loopmq.unsubscribe(s);   Note: uncomment the code to unsubscribe from the topic
```
 
6.***lopmq.disconnect ()***

This function is used to disconnect the device from the cloud or server. Disconnect does not stop the functionality of the device but disconnects it from the network. The device works fine locally but does not send any update to the internet.

```
// loopmq.disconnect();  Note: uncomment the code to disconnect the device
```

7.***JSON PARSER***

This function is used to create a JSON payload to be passed to the broker as payload. Please refer the [link](https://github.com/bblanchon/ArduinoJson/wiki/Compatibility-issues) for any compalibility issues.

```
  StaticJsonBuffer<200> jsonBuffer;               //  Inside the brackets, 200 is the size of the pool in bytes.If the JSON object is more complex, you need to increase that value. 
  
  JsonObject& root = jsonBuffer.createObject();   // It's a reference to the JsonObject, the actual bytes are inside the JsonBuffer with all the other nodes of the object tree.
  root["command"] = "sensor";                     // Add values in the object, add the objects you want to add to the JSON in the form of root["key'] = value;
  root["number"]= var;
  
  JsonObject& data= root.createNestedObject("data"); // nested JSON 
  data["number"]=var;                                // Add data["key"]= value
  data["Litmus"]="Loop"; 
```